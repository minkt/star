﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Star
{
    class Vulture : Unit
    {

        public int mineUpgrade = 0; // 업그레이드 안된상태 0이면 업그레이드 안된상태,
                                    //1이면 업그레이드 된상태
        int spmine = 0;
         
        public void spiderMine() //업그레이드
        {
            mineUpgrade = 1;
            spmine = 3;
        }


        public string mine(int x) 
        {
            if (x > 0)
            {
                if (mineUpgrade == 1 && spmine > 0)
                {
                    if (spmine > x)
                    {
                        spmine = spmine - x;
                        return "남은 마인 갯수는 " + spmine + "입니다";

                    }
                    else
                        return "남은 마인 갯수보다 심으려는 마인갯수가 많습니다.";

                }

                else
                    return "업그레이드가 아직 되지 않았습니다.";

            }
            else
                return "음수는 적을수 없습니다.";

        }



        public override void MakeSound()
        {
            Console.WriteLine("찰칵~~");
        }
    }
}
